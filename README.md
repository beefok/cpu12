# CPU12 README

!["pr"](/core_placeroute.png)

This project is very new so the documentation and commenting is at a bare minimum, sorry about that!

A basic description of the project is that it is a minimalistic 12-bit CPU.
It has 8 12-bit general purpose registers to work work with.

```
   ip       instruction pointer
   r0-r7    general purpose registers
```

There are plenty of neat programming tricks implemented within the encoding of the instructions. In time I hope to document these as best as I can.

The basic encoding of instructions is a 12-bit word. It is built up from 4 3-bit 'nibbles'. So we can use octals to encode them much like hex would be used for 4-bit nibbles.

```
in binary: nnnmmmxxxyyy

in octal:  nmxy
```

In octal we can encode the full instruction set this way:

The basic encoding of instructions has two opcode fields, m and n, and two instruction operands, x and y.

x is always a register, rx, from r0-7 which in general is source 0 and if the instruction writes to the register, is the destination register.
y is either a register, ry, from r0-7 which is source 1, or an immediate value which will be explained further along.

Instruction m and n encodings:
```
m
0-3jjj j
4nxy ld  st  jr  asr  lsl  lsr  rol  ror
5nxy ldi sti jri asri lsli lsri roli rori
6nxy eq  lr  sub add  mv   and  or   xor
7nxy eqi lri gri addi mi   andi ori  xori
 n-> 0   1   2   3    4    5    6    7
```

The jump instruction is a relative jump with a range of -2048 to +2047.

```
0003  j +3     # uhh, just jump 3 words ahead for whatever reason

# simple loop example.. 'multiply' r0 by 2 over and over again
6300  add r0, r0
3777  j   -1
```

A jump of 0 would halt the machine since it can't move forward, so I encoded it with the interrupt/return from interrupt instruction

```
0000 : brk : if in bios mode, jump to user mode and begin using user ip register
           : else, jump to bios mode and reset bios ip register to 0
```

BIOS and USER are separate code spaces of 4K x 12 words each. This allows you to handle more code and also separate system runtime code from user code.
They each have an instruction pointer register to allow quick interrupt handling and returns.

If you're in BIOS mode and you call the brk instruction, it resets the BIOS instruction pointer and starts using the USER instruction pointer
If you're in USER mode and you call the brk instruction, it does not reset the USER instruction pointer and starts using the BIOS instruction pointer

The reasoning behind this is that the BIOS instruction pointer should always go back to address 0000 on interrupts -- the BIOS will then decide what interrupt to handle with software instead of vectored interrupts.
Once the BIOS is done, it calls the BRK again to return to USER mode and the USER mode goes straight back to whatever it was working on.
Please also note that while in BIOS mode, all interrupts (except reset) are ignored. Only in USER space will they work, so it's on the BIOS to quickly get done its work..

ld and st are load register and store register

```
40xy : ld rx, ry : rx = [ry]

41xy : st rx, ry : [ry] = rx
```

asr/lsl/lsr/rol/ror are the usual shift instructions with a barrel shifter of 0-12 steps

```
43xy : asr rx, ry : rx = rx asr ry
```

ldi / sti are load and store register with immediate address

```
50xy 1234 : ldi rx, ry, #1234 : rx = [ry + 1234]

51xy 1234 : sti rx, ry, #1234 : [ry + 1234] = rx
```

asri/lsli/lsri/roli/rori are the shift instructions using an encoded immediate, you can shift from 1 to 12 steps, for example:

```
54x1 : lsl rx, #2 : rx = rx lsl #2

56x0 : rol rx, #1 : rx = rx ror #1

53x3 : asr rx, #4 : rx = rx asr #4
```

The next set of instructions are Conditionals and ALU based

Conditionals skip the next instruction if the condition is met from the comparision.

```
60xy : eq rx, ry : skip the next instruction if rx = ry

61xy : lr rx, ry : skip the next instruction if rx < ry
```

You might be wondering why there isn't a gr rx, ry ? Well that's because lr is equivalent..

```
lr ry, rx <-> gr rx, ry
```

So what's the point of wasting an instruction opcode?
However, to be fair, the immediate encodings of the conditionals includes a gri in place of what could have been a subi instruction.

Now lets see these instructions actually do something useful.
You can chain these conditionals with any other instruction, for instance:
```
60xy  eq rx, ry
0300  j  +300
```

The jump will only execute if rx and ry are equal. If they are not equal it will still process the instruction as a nop instruction (wasted cycle)
The addition of the conditional instructions allows you to basically make any instruction conditional.

The last 6 instructions are alu based: sub, add, mv, and, or, xor

```
62xy : sub rx, ry : rx = rx - ry

63xy : add rx, ry : rx = rx + ry

64xy : mv  rx, ry : rx = ry

65xy : and rx, ry : rx = rx & ry

66xy : or  rx, ry : rx = rx | ry

67xy : xor rx, ry : rx = rx ^ ry
```

Move Register is pretty self explanitory, but also important to have since there's no other way to put a clean copy into a new register.

and/or/xor work as they usually do, they are bitwise logical operators.

Lastly we have the immediate encodings of the Conditional and ALU instructions.
Please note that subi would be a pointless instruction because addi can do everything that subi could have done. Consider 2's complement and you will see that as well.
The neat thing about the immediate encodings of the Conditional and ALU instructions is that one of the encodings allows you to use an external immediate:

immediate 0-3 are positive 0-3, immediate 5-7 are -3, -2, -1, and immediate 4 is an external immediate from memory right after the instruction

```
examples of external immediate:

74x4 1234 : mi rx, #1234 : rx = #1234

67x4 4315 : xor rx, #4315 : rx = rx xor #4315
```

The neat thing too is that you also have a few internal immediates that don't waste code space when you use them.

Lets say you need to increment a register.

```
73x1  :  addi rx, +1 : rx = rx + 1
```
This is encoded in a single instruction!

```
77x7  :  xor rx, -1 : rx = not rx
```
(Can you see why this works?)

You can even make a stack by using a register as a stack pointer

example of push instruction: Lets say r7 is your stack pointer and r0 is the value you want to push to the stack

```
psh r0:
4107  st    r0, r7
7377  addi  r7, -1
```

example of pop instruction: Lets say r7 is your stack pointer and r0 is where you want to read in the stack item

```
pop r0:
7371  addi r7, +1
4007  ld   r0, r7
```

Notice how easy it is to figure out how to encode/decode instructions from the octal nibbles?
You can quickly assemble/disassemble code because of that.


More documentation effort to come.

