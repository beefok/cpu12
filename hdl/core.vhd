library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.cpu_pkg.all;

entity core is port (
   rst, irq, clk : in   std_logic;
   we, ce, oe   : out   std_logic;
   dev          : out   std_logic_vector(1 downto 0);
   adr          : out   bits;
   dio          : inout bits
);
end core;

architecture rtl of core is

-- component data
-- port (
--    rs, ck      : in  std_logic;
--    cs          : in  ctrl_t;
--    ma          : in  bits;
--    mq          : in  bits;
--    mi          : out bits
-- );
-- end component;

   component ctrl
   port (
      iw : in  bits;
      st : in  std_logic_vector(1 downto 0);
      sk : in  std_logic;
      um : in  std_logic;
      cs : out ctrl_t
   );
   end component;

   component regs
   port (
      rs, ck, wb  : in  std_logic;
      si, sx, sy  : in  std_logic_vector( 2 downto 0);
      di          : in  bits;
      rx, ry      : out bits
   );
   end component;

   component immt
   port (
      iw : in  bits;
      jm : out bits;
      sh : out bits;
      im : out bits
   );
   end component;

   component exec
   port (
      cs   : in  ctrl_t;
      ea   : in  bits;
      eb   : in  bits;
      sk   : out std_logic;
      ex   : out bits
   );
   end component;

   signal cs : ctrl_t;

   signal iw : bits;
   signal iq : bits;
   signal ip : bits; -- mux-selected ip
   signal bp : bits; -- bios ip
   signal up : bits; -- user ip
   signal um : std_logic;

   signal ma : bits;
   signal mi : bits;

   signal rs : std_logic_vector(1 downto 0);
   signal ir : std_logic;
   signal st : std_logic_vector(1 downto 0);

   -- execution unit signals
   signal ex : bits;
   signal ea : bits;
   signal eb : bits;
   signal sk : std_logic;
   signal sq : std_logic;

   -- register file signals
   signal sx : std_logic_vector(2 downto 0);
   signal sy : std_logic_vector(2 downto 0);
   signal wb : std_logic;
   signal ri : bits;
   signal rx : bits;
   signal ry : bits;

   -- immediate table signals
   signal jm : bits;
   signal sh : bits;
   signal im : bits;

begin

   -- synchronized reset
   process (rst, clk)
   begin
      if (rst = '0') then
         rs <= (others => '0');
         ir <= '0';
      elsif (rising_edge(clk)) then
         rs <= rs(0) & rst;
         ir <= not irq;
      end if;
   end process;

   -- data memory bus
-- u0 : data_ex port map ( rs => rs(1), ck => ck, cs => cs, ma => ma, mq => rx, mi => mi );
   process (rs(1), clk)
   begin

      if (rs(1) = '0') then
         ce <= '0';
         oe <= '0';
         we <= '0';
         dev <= (others => '0');
         adr <= (others => '0');
         dio <= (others => 'Z');
         mi  <= (others => '0');

      elsif (rising_edge(clk)) then

         adr <= ma;

         case (cs.mb) is
            when mb_cr =>
               ce <= '1'; oe <= '1'; we <= '0';
               dio <= (others => 'Z');
               mi  <= dio;

               -- select between bios and user rom
               if ((cs.ir = ir_gb) or (cs.ir = ir_no and um = '0')) then
                  dev <= "00"; -- bios rom
               elsif ((cs.ir = ir_gu) or (cs.ir = ir_no and um = '1')) then
                  dev <= "01"; -- user rom
               end if;

            when mb_dr =>
               ce <= '1'; oe <= '1'; we <= '0';
               dio <= (others => 'Z');
               mi  <= dio;

               -- select between data bank (r0-r3) and (r4-r7)
               if (sx = o"0" or sx = o"1" or sx = o"2" or sx = o"3") then
                  dev <= "10"; -- r0-r3 ram
               else
                  dev <= "11"; -- r4-r7 ram
               end if;

            when mb_dw =>
               ce <= '1'; oe <= '0'; we <= '1';
               dio <= rx;
               mi <= mi;

               -- select between data bank (r0-r3) and (r4-r7)
               if (sx = o"0" or sx = o"1" or sx = o"2" or sx = o"3") then
                  dev <= "10"; -- r0-r3 ram
               else
                  dev <= "11"; -- r4-r7 ram
               end if;

            when others =>
               ce <= '0'; oe <= '0'; we <= '0';
               dev <= (others => '0');
               dio <= (others => 'Z');
               mi <= mi;

         end case;

      end if;

   end process;

   -- control unit
   u1 : ctrl port map ( iw => iw, st => st, sk => sk, um => um, cs => cs );

   -- select what the instruction word is
   iw <= o"0000" when rs(1) = '0' or ir = '1' else
         mi      when st = "00" else
         iq;

   -- select instruction pointer
   ip <= --o"0000" when rst = '0' or rs = '0' else
         bp      when (cs.ir = ir_gb) or (cs.ir = ir_no and um = '0') else
         up;

   -- select memory address
   with cs.ma select ma <=
      ex when ma_ex,
      ip when others;

   -- register updates
   process (rs(1), clk)
   begin
      if (rs(1) = '0') then
         iq <= (others => '0');
         bp <= (others => '0');
         up <= (others => '0');
         um <= '0';
         st <= (others => '0');
         sk <= '0';
      elsif (rising_edge(clk)) then

         -- update user mode flag
         case (cs.ir) is
            when ir_gb => um <= '0';
            when ir_gu => um <= '1';
            when others => null;
         end case;

         -- update ip
         if (cs.ip = ip_nx) then
            -- on interrupt (to bios mode)
            if (cs.ir = ir_gb) then
               bp <= ma + '1';
            -- on return from interrupt (to user mode)
            elsif (cs.ir = ir_gu) then
               bp <= o"0000";
               up <= ma + '1';
            -- otherwise dependent upon user mode flag
            else
               -- if in bios mode
               if (um = '0') then
                  bp <= ma + '1';
               -- if in user mode
               else
                  up <= ma + '1';
               end if;
            end if;
         end if;

         -- update stored instruction word
         if (cs.st = st_1) then
            iq <= mi;
         end if;

         -- update next state transition
         case (cs.st) is
            when st_0   => st <= "00";
            when st_1   => st <= "01";
            when st_2   => st <= "10";
            when others => st <= "11";    -- bad state, you will get stuck here!
         end case;

         -- update skip state
         sk <= sq;

      end if;
   end process;

   -- register file
   u2 : regs port map ( rs => rs(1), ck => clk, wb => wb, si => sx, sx => sx, sy => sy, di => ri, rx => rx, ry => ry );

   -- register write input select
   with cs.ri select ri <=
      ip when ri_ip,
      ex when others;

   -- register write signal
   wb <= '1' when cs.wb = wb_wr else '0';

   -- register operand fields
   sx <= iw(5 downto 3);
   sy <= iw(2 downto 0);

   -- immediate table
   u3 : immt port map ( iw => iw, jm => jm, sh => sh, im => im );

   -- execution unit (alu)
   u4 : exec port map ( cs => cs, ea => ea, eb => eb, sk => sq, ex => ex );

   -- execution unit select a
   with cs.ea select ea <=
      ip when ea_ip,
      mi when ea_mi,
      rx when others;

   -- execution unit select b
   with cs.eb select eb <=
      jm when eb_jm,
      sh when eb_sh,
      im when eb_im,
      mi when eb_mi,
      ry when others;

end rtl;