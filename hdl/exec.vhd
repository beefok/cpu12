library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.cpu_pkg.all;

entity exec is port (
   cs   : in  ctrl_t;
   ea   : in  bits;
   eb   : in  bits;
   sk   : out std_logic;
   ex   : out bits
);
end exec;

architecture rtl of exec is

   signal c_ext, c_zer, c_asr, c_lsl, c_lsr, c_rol, c_ror, c_alu : bits;

begin

   -- zero extension for lsl/lsr
   c_zer <= o"0000";

   -- sign extension for asr
   c_ext <= o"7777" when ea(11) = '1' else c_zer;

   -- arith. shift right
   with eb select c_asr <=
                               ea(11 downto  0) when o"0000",
      c_ext(           0) &    ea(11 downto  1) when o"0001",
      c_ext( 1 downto  0) &    ea(11 downto  2) when o"0002",
      c_ext( 2 downto  0) &    ea(11 downto  3) when o"0003",
      c_ext( 3 downto  0) &    ea(11 downto  4) when o"0004",
      c_ext( 4 downto  0) &    ea(11 downto  5) when o"0005",
      c_ext( 5 downto  0) &    ea(11 downto  6) when o"0006",
      c_ext( 6 downto  0) &    ea(11 downto  7) when o"0007",
      c_ext( 7 downto  0) &    ea(11 downto  8) when o"0010",
      c_ext( 8 downto  0) &    ea(11 downto  9) when o"0011",
      c_ext( 9 downto  0) &    ea(11 downto 10) when o"0012",
      c_ext(11 downto  0)                       when others;

   -- logical shift left
   with eb select c_lsl <=
         ea(11 downto  0)                       when o"0000",
         ea(10 downto  0) & c_zer(11          ) when o"0001",
         ea( 9 downto  0) & c_zer(11 downto 10) when o"0002",
         ea( 8 downto  0) & c_zer(11 downto  9) when o"0003",
         ea( 7 downto  0) & c_zer(11 downto  8) when o"0004",
         ea( 6 downto  0) & c_zer(11 downto  7) when o"0005",
         ea( 5 downto  0) & c_zer(11 downto  6) when o"0006",
         ea( 4 downto  0) & c_zer(11 downto  5) when o"0007",
         ea( 3 downto  0) & c_zer(11 downto  4) when o"0010",
         ea( 2 downto  0) & c_zer(11 downto  3) when o"0011",
         ea( 1 downto  0) & c_zer(11 downto  2) when o"0012",
         ea(           0) & c_zer(11 downto  1) when o"0013",
                            c_zer(11 downto  0) when others;

   -- logical shift right
   with eb select c_lsr <=
                               ea(11 downto  0) when o"0000",
      c_zer(           0) &    ea(11 downto  1) when o"0001",
      c_zer( 1 downto  0) &    ea(11 downto  2) when o"0002",
      c_zer( 2 downto  0) &    ea(11 downto  3) when o"0003",
      c_zer( 3 downto  0) &    ea(11 downto  4) when o"0004",
      c_zer( 4 downto  0) &    ea(11 downto  5) when o"0005",
      c_zer( 5 downto  0) &    ea(11 downto  6) when o"0006",
      c_zer( 6 downto  0) &    ea(11 downto  7) when o"0007",
      c_zer( 7 downto  0) &    ea(11 downto  8) when o"0010",
      c_zer( 8 downto  0) &    ea(11 downto  9) when o"0011",
      c_zer( 9 downto  0) &    ea(11 downto 10) when o"0012",
      c_zer(10 downto  0) &    ea(          11) when o"0013",
      c_zer(11 downto  0)                       when others;

   -- rotate left (breaks any higher than 12 shifts)
   with eb select c_rol <=
         ea(11 downto  0)                       when o"0000",
         ea(10 downto  0) &    ea(          11) when o"0001",
         ea( 9 downto  0) &    ea(11 downto 10) when o"0002",
         ea( 8 downto  0) &    ea(11 downto  9) when o"0003",
         ea( 7 downto  0) &    ea(11 downto  8) when o"0004",
         ea( 6 downto  0) &    ea(11 downto  7) when o"0005",
         ea( 5 downto  0) &    ea(11 downto  6) when o"0006",
         ea( 4 downto  0) &    ea(11 downto  5) when o"0007",
         ea( 3 downto  0) &    ea(11 downto  4) when o"0010",
         ea( 2 downto  0) &    ea(11 downto  3) when o"0011",
         ea( 1 downto  0) &    ea(11 downto  2) when o"0012",
         ea(           0) &    ea(11 downto  1) when o"0013",
                            c_zer(11 downto  0) when others;

   -- rotate right (breaks any higher than 12 shifts)
   with eb select c_ror <=
                               ea(11 downto  0) when o"0000",
         ea(           0) &    ea(11 downto  1) when o"0001",
         ea( 1 downto  0) &    ea(11 downto  2) when o"0002",
         ea( 2 downto  0) &    ea(11 downto  3) when o"0003",
         ea( 3 downto  0) &    ea(11 downto  4) when o"0004",
         ea( 4 downto  0) &    ea(11 downto  5) when o"0005",
         ea( 5 downto  0) &    ea(11 downto  6) when o"0006",
         ea( 6 downto  0) &    ea(11 downto  7) when o"0007",
         ea( 7 downto  0) &    ea(11 downto  8) when o"0010",
         ea( 8 downto  0) &    ea(11 downto  9) when o"0011",
         ea( 9 downto  0) &    ea(11 downto 10) when o"0012",
         ea(10 downto  0) &    ea(          11) when o"0013",
      c_zer(11 downto  0)                       when others;

   -- final alu output mux
   with cs.es select ex <=
      ea  +  eb when es_add,
      ea  -  eb when es_sub,
      ea and eb when es_and,
      ea or  eb when es_orr,
      ea xor eb when es_xor,
          c_asr when es_asr,
          c_lsl when es_lsl,
          c_lsr when es_lsr,
          c_rol when es_rol,
          c_ror when es_ror,
             eb when others;

   -- comparision for skip
   sk <= '1' when ((cs.sk = sk_eq) and (ea = eb)) or
                  ((cs.sk = sk_lr) and (ea < eb)) or
                  ((cs.sk = sk_gr) and (ea > eb)) else
         '0';

end rtl;