library ieee;
use ieee.std_logic_1164.all;

package cpu_pkg is

   subtype bits is std_logic_vector(11 downto 0);

   type ma_t is (ma_ip, ma_ex);
   type mb_t is (mb_cr, mb_dr, mb_dw);
   type ip_t is (ip_nx, ip_no);
   type es_t is (es_mov, es_add, es_sub, es_and, es_orr, es_xor, es_asr, es_lsl, es_lsr, es_rol, es_ror);
   type ea_t is (ea_rx, ea_ip, ea_mi);
   type eb_t is (eb_ry, eb_jm, eb_mi, eb_sh, eb_im);
   type ri_t is (ri_ex, ri_ip);
   type wb_t is (wb_no, wb_wr);
   type sk_t is (sk_no, sk_eq, sk_lr, sk_gr);
   type st_t is (st_0, st_1, st_2);
   type ir_t is (ir_no, ir_gu, ir_gb);

   type ctrl_t is record
      ma : ma_t;
      mb : mb_t;
      ip : ip_t;
      es : es_t;
      ea : ea_t;
      eb : eb_t;
      ri : ri_t;
      wb : wb_t;
      sk : sk_t;
      st : st_t;
      ir : ir_t;
   end record;

end package;