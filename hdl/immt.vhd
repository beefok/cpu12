library ieee;
use ieee.std_logic_1164.all;
use work.cpu_pkg.all;

entity immt is port (
   iw : in  bits;
   jm : out bits;
   sh : out bits;
   im : out bits
);
end immt;

architecture rtl of immt is

   signal sy : std_logic_vector(2 downto 0);

begin

   -- 11-bit jump relative address, 1-bit extended to 12-bit
   jm <= iw(10) & iw(10 downto 0);

   -- select based on operand y
   sy <= iw(2 downto 0);

   -- internal immediates for alui instructions
   with sy select im <=
      o"0001" when o"1",
      o"0002" when o"2",
      o"0003" when o"3",
      o"7775" when o"5",
      o"7776" when o"6",
      o"7777" when o"7",
      o"0000" when others;

   -- shift immediates
   with sy select sh <=
      o"0002" when o"1",
      o"0003" when o"2",
      o"0004" when o"3",
      o"0005" when o"4",
      o"0006" when o"5",
      o"0007" when o"6",
      o"0010" when o"7",
      o"0001" when others;

end rtl;