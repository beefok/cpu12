-- note: temporary data interface for simulation
-- the real deal will have to use an external memory unless I get IP access to sram(s)

library ieee;
use std.textio.all;
use ieee.std_logic_textio.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.cpu_pkg.all;

entity data is port (
   rs, ck      : in  std_logic;
   cs          : in  ctrl_t;
   ma          : in  bits;
   mq          : in  bits;
   mi          : out bits
);
end data;

architecture rtl of data is

   -- memory types
   type data_t is array (0 to 4095) of bits;

   impure function InitRomFromFile (RomFileName : in string) return data_t is
      FILE romfile : text is in RomFileName;
      variable RomFileLine : line;
      variable rom : data_t;
      variable i : integer;
   begin
      i := 0;

      -- read in rom file
      while not endfile(romfile) loop
         readline(romfile, RomFileLine);
         oread(RomFileLine, rom(i));
         i := i + 1;
      end loop;

      -- if rom isn't finished, clear rom
      while i < 4096 loop
         if (i >= 512 and i < 1024) then
            rom(i) := conv_std_logic_vector(4095 - (i - 512), 12);
         else
            rom(i) := o"0000";
         end if;
         i := i + 1;
      end loop;
      return rom;
   end function;

   -- inst. memory blocks (and initialize bios for simulation)
   shared variable bios_mem : data_t := InitRomFromFile("bios.octal");

begin

   process (rs, ck) begin
      if (rs = '0') then
         mi  <= (others => '0');
      elsif (rising_edge(ck)) then
         if (cs.mb = mb_dw) then
            bios_mem(conv_integer(ma)) := mq;
            mi <= mq;
         else
            mi <= bios_mem(conv_integer(ma));
         end if;
      end if;
   end process;

end rtl;