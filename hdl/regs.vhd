library ieee;
use ieee.std_logic_1164.all;
use work.cpu_pkg.all;

entity regs is port (
   rs, ck, wb  : in  std_logic;
   si, sx, sy  : in  std_logic_vector(2 downto 0);
   di          : in  bits;
   rx, ry      : out bits
);
end regs;

architecture rtl of regs is

   -- register file flip flops
   signal   r0, r1, r2, r3 ,
            r4, r5, r6, r7 : bits;

begin

   -- register file write in
   process (rs, ck)
   begin
      if (rs = '0') then
         r0 <= (others => '0');
         r1 <= (others => '0');
         r2 <= (others => '0');
         r3 <= (others => '0');
         r4 <= (others => '0');
         r5 <= (others => '0');
         r6 <= (others => '0');
         r7 <= (others => '0');
      elsif (rising_edge(ck)) then
         if (wb = '1') then
            case (si) is
               when o"0"   => r0 <= di;
               when o"1"   => r1 <= di;
               when o"2"   => r2 <= di;
               when o"3"   => r3 <= di;
               when o"4"   => r4 <= di;
               when o"5"   => r5 <= di;
               when o"6"   => r6 <= di;
               when o"7"   => r7 <= di;
               when others => null;
            end case;
         end if;
      end if;
   end process;

   -- register file output x
   rx <= r0 when sx = o"0" else
         r1 when sx = o"1" else
         r2 when sx = o"2" else
         r3 when sx = o"3" else
         r4 when sx = o"4" else
         r5 when sx = o"5" else
         r6 when sx = o"6" else
         r7;

   -- register file output y
   ry <= r0 when sy = o"0" else
         r1 when sy = o"1" else
         r2 when sy = o"2" else
         r3 when sy = o"3" else
         r4 when sy = o"4" else
         r5 when sy = o"5" else
         r6 when sy = o"6" else
         r7;

end rtl;