----------------------------------------------------------------------------------
-- state transition tables
----------------------------------------------------------------------------------
-- op    #  curr.state  ma    mb    ip    es    ea    eb    ri    sk    st    ir
-- 0000  #  irq         ip.0  cr    +1    mov   rx    ry    --    --    0     gb
--       #  rei         ip.1  cr    +1    mov   rx    ry    --    --    0     gu
-- jjjj  #  j           ex    cr    +1    add   ip    jm    --    --    0     -
-- 40xy  #  ld.0        ex    dr    --    mov   rx    ry    --    --    1     -
--       #  ld.1        ip    cr    +1    mov   rx    mi    ex    --    0     -
-- 41xy  #  st.0        ex    dw    --    mov   rx    ry    --    --    1     -
--       #  st.1        ip    cr    +1    mov   rx    ry    --    --    0     -
-- 42xy  #  jr          ex    cr    +1    mov   rx    ry    ip    --    0     -
-- 43xy  #  asr         ip    cr    +1    asr   rx    ry    ex    --    0     -
-- 44xy  #  lsl         ip    cr    +1    lsl   rx    ry    ex    --    0     -
-- 45xy  #  lsr         ip    cr    +1    lsr   rx    ry    ex    --    0     -
-- 46xy  #  rol         ip    cr    +1    rol   rx    ry    ex    --    0     -
-- 47xy  #  ror         ip    cr    +1    ror   rx    ry    ex    --    0     -
-- 50xy  #  ldi.0       ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  ldi.1       ex    dr    --    add   mi    ry    --    --    2     -
--       #  ldi.2       ip    cr    +1    mov   rx    mi    ex    --    0     -
-- 51xy  #  sti.0       ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  sti.1       ex    dw    --    add   mi    ry    --    --    2     -
--       #  sti.2       ip    cr    +1    mov   rx    ry    --    --    0     -
-- 52xy  #  jri.0       ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  jri.1       ex    cr    +1    mov   rx    mi    ip    --    0     -
-- 53xy  #  asri        ip    cr    +1    asr   rx    sh    ex    --    0     -
-- 54xy  #  lsli        ip    cr    +1    lsl   rx    sh    ex    --    0     -
-- 55xy  #  lsri        ip    cr    +1    lsr   rx    sh    ex    --    0     -
-- 56xy  #  roli        ip    cr    +1    rol   rx    sh    ex    --    0     -
-- 57xy  #  rori        ip    cr    +1    ror   rx    sh    ex    --    0     -
-- 60xy  #  eq          ip    cr    +1    mov   rx    ry    --    eq    0     -
-- 61xy  #  lr          ip    cr    +1    mov   rx    ry    --    lr    0     -
-- 62xy  #  sub         ip    cr    +1    sub   rx    ry    ex    --    0     -
-- 63xy  #  add         ip    cr    +1    add   rx    ry    ex    --    0     -
-- 64xy  #  mv          ip    cr    +1    mov   rx    ry    ex    --    0     -
-- 65xy  #  and         ip    cr    +1    and   rx    ry    ex    --    0     -
-- 66xy  #  or          ip    cr    +1    orr   rx    ry    ex    --    0     -
-- 67xy  #  xor         ip    cr    +1    xor   rx    ry    ex    --    0     -
-- 70x4  #  eqi.0  ext  ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  eqi.1  ext  ip    cr    +1    mov   rx    mi    --    eq    0     -
-- 70xy  #  eqi    imm  ip    cr    +1    mov   rx    im    --    eq    0     -
-- 71x4  #  lri.0  ext  ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  lri.1  ext  ip    cr    +1    mov   rx    mi    --    lr    0     -
-- 71xy  #  lri    imm  ip    cr    +1    mov   rx    im    --    lr    0     -
-- 72x4  #  gri.0  ext  ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  gri.1  ext  ip    cr    +1    mov   rx    mi    --    gr    0     -
-- 72xy  #  gri    imm  ip    cr    +1    mov   rx    im    --    gr    0     -
-- 73x4  #  addi.0 ext  ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  addi.1 ext  ip    cr    +1    add   rx    mi    ex    --    0     -
-- 73xy  #  addi   imm  ip    cr    +1    add   rx    im    ex    --    0     -
-- 74x4  #  mi.0   ext  ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  mi.1   ext  ip    cr    +1    mov   rx    mi    ex    --    0     -
-- 74xy  #  mi     imm  ip    cr    +1    mov   rx    im    ex    --    0     -
-- 75x4  #  andi.0 ext  ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  andi.1 ext  ip    cr    +1    and   rx    mi    ex    --    0     -
-- 75xy  #  andi   imm  ip    cr    +1    and   rx    im    ex    --    0     -
-- 76x4  #  ori.0  ext  ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  ori.1  ext  ip    cr    +1    orr   rx    mi    ex    --    0     -
-- 76xy  #  ori    imm  ip    cr    +1    orr   rx    im    ex    --    0     -
-- 77x4  #  xori.0 ext  ip    cr    +1    mov   rx    ry    --    --    1     -
--       #  xori.1 ext  ip    cr    +1    xor   rx    mi    ex    --    0     -
-- 77xy  #  xori   imm  ip    cr    +1    xor   rx    im    ex    --    0     -
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.cpu_pkg.all;

entity ctrl is port (
   iw : in  bits;
   st : in  std_logic_vector(1 downto 0);
   sk : in  std_logic;
   um : in  std_logic;
   cs : out ctrl_t
);
end ctrl;

architecture rtl of ctrl is

   signal   op       : std_logic_vector(5 downto 0);
   signal   sx, sy   : std_logic_vector(2 downto 0);

   signal   c_st0, c_st1, c_st2, c_ext, c_imm : boolean;

   signal   c_irq , c_jmp , c_skp , c_user, c_bios,
            c_ld  , c_st  , c_jr  , c_sh  ,
            c_ldi , c_sti , c_jri , c_shi ,
            c_alu , c_alui, c_alue, c_cmpi, c_cmpe,
            c_eq  , c_eqi , c_eqe ,
            c_lr  , c_lri , c_lre ,
                    c_gri , c_gre : boolean;
   signal   c_asr , c_lsl , c_lsr , c_rol , c_ror ,
            c_asri, c_lsli, c_lsri, c_roli, c_rori,
            c_sub , c_add , c_and , c_or  , c_xor ,
                    c_addi, c_andi, c_ori , c_xori,
                    c_adde, c_ande, c_ore , c_xore: boolean;
begin

   -- instruction word portions
   op <= iw(11 downto 6);
   sx <= iw( 5 downto 3);
   sy <= iw( 2 downto 0);

   -- state detection
   c_st0 <= st = "00";
   c_st1 <= st = "01";
   c_st2 <= st = "10";

   -- immediate mode type
   c_ext <= sy = o"4";
   c_imm <= not c_ext;

   -- instruction decoding
   c_irq <= op = o"00" and sx = o"0" and sy = o"0";
   c_jmp <= op(5) = '0';
   c_skp <= sk = '1';
   c_user <= um = '1';
   c_bios <= not c_user;

   c_ld  <= op = o"40";
   c_st  <= op = o"41";
   c_jr  <= op = o"42";
   c_ldi <= op = o"50";
   c_sti <= op = o"51";
   c_jri <= op = o"52";

   c_sh   <= (              op = o"43" or op = o"44" or op = o"45" or op = o"46" or op = o"47");
   c_shi  <= (              op = o"53" or op = o"54" or op = o"55" or op = o"56" or op = o"57");
   c_alu  <= (op = o"62" or op = o"63" or op = o"64" or op = o"65" or op = o"66" or op = o"67");
   c_alui <= (op = o"72" or op = o"73" or op = o"74" or op = o"75" or op = o"76" or op = o"77") and c_imm;
   c_alue <= (op = o"72" or op = o"73" or op = o"74" or op = o"75" or op = o"76" or op = o"77") and c_ext;
   c_cmpi <= (op = o"70" or op = o"71" or op = o"72") and c_imm;
   c_cmpe <= (op = o"70" or op = o"71" or op = o"72") and c_ext;

   c_asr  <= op = o"43";
   c_asri <= op = o"53";
   c_lsl  <= op = o"44";
   c_lsli <= op = o"54";
   c_lsr  <= op = o"45";
   c_lsri <= op = o"55";
   c_rol  <= op = o"46";
   c_roli <= op = o"56";
   c_ror  <= op = o"47";
   c_rori <= op = o"57";

   c_eq   <= op = o"60";
   c_eqi  <= op = o"70" and c_imm;
   c_eqe  <= op = o"70" and c_ext;
   c_lr   <= op = o"61";
   c_lri  <= op = o"71" and c_imm;
   c_lre  <= op = o"71" and c_ext;
   c_sub  <= op = o"62";
   c_gri  <= op = o"72" and c_imm;
   c_gre  <= op = o"72" and c_ext;
   c_add  <= op = o"63";
   c_addi <= op = o"73" and c_imm;
   c_adde <= op = o"73" and c_ext;
   c_and  <= op = o"65";
   c_andi <= op = o"75" and c_imm;
   c_ande <= op = o"75" and c_ext;
   c_or   <= op = o"66";
   c_ori  <= op = o"76" and c_imm;
   c_ore  <= op = o"76" and c_ext;
   c_xor  <= op = o"67";
   c_xori <= op = o"77" and c_imm;
   c_xore <= op = o"77" and c_ext;

   -- memory address
   cs.ma <= ma_ex    when   (not c_skp       )  and
                           ((c_jmp           )  or
                            (c_ld   and c_st0)  or
                            (c_ldi  and c_st1)  or
                            (c_st   and c_st0)  or
                            (c_sti  and c_st1)  or
                            (c_jr            )  or
                            (c_jri  and c_st1)) else
            ma_ip;

   -- memory bus
   cs.mb <= mb_dw    when   (not c_skp       )  and
                           ((c_st   and c_st0)  or
                            (c_sti  and c_st1)) else
            mb_dr    when   (c_ld   and c_st0)  or
                            (c_ldi  and c_st1)  else
            mb_cr;

   -- instruction pointer
   cs.ip <= ip_no    when   (c_ld   and c_st0)  or
                            (c_ldi  and c_st1)  or
                            (c_st   and c_st0)  or
                            (c_sti  and c_st1)  else
            ip_nx;

   -- execute.s
   cs.es <= es_add   when   (c_jmp           )  or
                            (c_ldi  and c_st1)  or
                            (c_sti  and c_st1)  or
                            (c_add           )  or
                            (c_addi          )  or
                            (c_adde and c_st1)  else
            es_sub   when   (c_sub           )  else
            es_asr   when   (c_asr           )  or
                            (c_asri          )  else
            es_lsl   when   (c_lsl           )  or
                            (c_lsli          )  else
            es_lsr   when   (c_lsr           )  or
                            (c_lsri          )  else
            es_rol   when   (c_rol           )  or
                            (c_roli          )  else
            es_ror   when   (c_ror           )  or
                            (c_rori          )  else
            es_and   when   (c_and           )  or
                            (c_andi          )  or
                            (c_ande and c_st1)  else
            es_orr   when   (c_or            )  or
                            (c_ori           )  or
                            (c_ore  and c_st1)  else
            es_xor   when   (c_xor           )  or
                            (c_xori          )  or
                            (c_xore and c_st1)  else
            es_mov;

   -- execute.a
   cs.ea <= ea_ip    when   (c_jmp           )  else
            ea_mi    when   (c_ldi  and c_st1)  or
                            (c_sti  and c_st1)  else
            ea_rx;

   -- execute.b
   cs.eb <= eb_jm    when   (c_jmp           )  else
            eb_mi    when   (c_ld   and c_st1)  or
                            (c_ldi  and c_st2)  or
                            (c_jri  and c_st1)  or
                            (c_cmpe and c_st1)  or
                            (c_alue and c_st1)  else
            eb_sh    when   (c_shi           )  else
            eb_im    when   (c_cmpi          )  or
                            (c_alui          )  else
            eb_ry;

   -- register in mux
   cs.ri <= ri_ip    when   (c_jr            )  or
                            (c_jri  and c_st1)  else
            ri_ex;

   -- write back
   cs.wb <= wb_wr    when   (not c_skp       )  and
                           ((c_ld   and c_st1)  or
                            (c_jr            )  or
                            (c_sh            )  or
                            (c_ldi  and c_st2)  or
                            (c_jri  and c_st1)  or
                            (c_shi           )  or
                            (c_alu           )  or
                            (c_alue and c_st1)  or
                            (c_alui          )) else
            wb_no;

   -- skip next instruction
   cs.sk <= sk_eq    when   (c_eq            )  or
                            (c_eqi           )  or
                            (c_eqe  and c_st1)  else
            sk_lr    when   (c_lr            )  or
                            (c_lri           )  or
                            (c_lre  and c_st1)  else
            sk_gr    when   (c_gri           )  or
                            (c_gre  and c_st1)  else
            sk_no;

   -- next state
   cs.st <= st_1     when  ((c_st0           )  and
                           ((c_ld            )  or
                            (c_st            )  or
                            (c_ldi           )  or
                            (c_jri           )  or
                            (c_eqe           )  or
                            (c_lre           )  or
                            (c_gre           )  or
                            (c_alue          )))else
            st_2     when  ((c_st1           )  and
                           ((c_ldi           )  or
                            (c_sti           )))else
            st_0;

   -- interrupt mode
   cs.ir <= ir_gb    when   (c_irq and c_st0 and c_user)  else
            ir_gu    when   (c_irq and c_st0 and c_bios)  else
            ir_no;

end rtl;